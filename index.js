const express = require('express');

const app = express();
const mongoose = require('mongoose');
const authRoutes = require('./routes/auth');
const userRoutes = require('./routes/user');
const stockRoutes = require('./routes/stock')

mongoose.connect('mongodb+srv://dbUser:M9tQSnJgzlnS2JfZ@cluster0.aknu4.mongodb.net/test?retryWrites=true&w=majority',
    { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('Connexion à MongoDB réussie !'))
    .catch(() => console.log('Connexion à MongoDB échouée !'));

// ALLOW CORS FROM ALL ORIGINS
app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
});

// USE THE BODY PARSER FOR ALL JSON
app.use(express.json());

// CALL THE ROUTERS FOR EACH ROUTE
app.use('/api/user', userRoutes);
app.use('/auth', authRoutes);
app.use('/stock', stockRoutes)

module.exports = app;