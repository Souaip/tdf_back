const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const stockSchema = mongoose.Schema({
  city: { type: String, required: true, unique: true },
  stock: { type: Object, required: true }
}, {
  autoIndex: false,
});

stockSchema.plugin(uniqueValidator);

module.exports = mongoose.model('stock', stockSchema);