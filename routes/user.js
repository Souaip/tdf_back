const express = require('express');

const router = express.Router();

const userController = require('../controllers/user');
const auth = require('../middleware/auth');

router.get('/', auth, userController.getAllUsers);
router.get('/:email', auth, userController.getUserByEmail);
router.get('/:id', auth, userController.getUserById);
router.put('/:id', auth, userController.updateUserById);
router.delete('/:id', auth, userController.deleteUserById);

module.exports = router;