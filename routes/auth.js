const express = require('express');

const router = express.Router();

const userController = require('../controllers/user');

const bodyParser = require('body-parser')

// create application/json parser
let jsonParser = bodyParser.json()

// create application/x-www-form-urlencoded parser
let urlencodedParser = bodyParser.urlencoded({ extended: false })

router.post('/signup', urlencodedParser, userController.signup);
router.post('/login', urlencodedParser, userController.login);

module.exports = router;