const express = require('express');

const router = express.Router();

const userController = require('../controllers/stock');
const auth = require('../middleware/auth');

const bodyParser = require('body-parser')

// create application/json parser
let jsonParser = bodyParser.json()

// create application/x-www-form-urlencoded parser
let urlencodedParser = bodyParser.urlencoded({ extended: false })

router.get('/getAllShop', userController.getAllShop);
router.post('/updateShop', jsonParser, userController.updateShop)

module.exports = router;