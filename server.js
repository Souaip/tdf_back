const http = require('http');
const app = require('./index');
const ws = require('./middleware/websocket');

let port = '3000';
if (process.env.PORT) {
  port = process.env.PORT;
}

app.set('port', port);

const httpserver = http.createServer(app);

ws.launchwsserver(httpserver);

const errorHandler = (error) => {
  if (error.syscall !== 'listen') {
    throw error;
  }
  const address = httpserver.address();
  const bind = typeof address === 'string' ? `pipe ${address}` : `port: ${port}`;
  switch (error.code) {
    case 'EACCES':
      console.error(`${bind} requires elevated privileges.`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(`${bind} is already in use.`);
      process.exit(1);
      break;
    default:
      throw error;
  }
};

httpserver.on('error', errorHandler);
httpserver.on('listening', () => {
  const address = httpserver.address();
  const bind = typeof address === 'string' ? `pipe ${address}` : `port ${port}`;
  console.log(`Listening on ${bind}`);
});

httpserver.listen(port);