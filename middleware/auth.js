const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1];
    jwt.verify(token, 'STEVEBABAISMYSECRETANDIHAVENOSHAMEWHATSOEVER');
    // const userId = decodedToken.userId;
    next();
  } catch {
    res.status(401).json({ error: 'Authentication error' });
  }
};