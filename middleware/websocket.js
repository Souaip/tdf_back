const webSocket = require('ws');

const clientArr = [];

function launchwsserver(httpserver) {
  const wss = new webSocket.Server({ server: httpserver, path: '/websocket' });

  let id = 0;

  wss.on('connection', (ws) => {
    ws.on('message', (message) => {
      const messageArr = message.split(';');
      if (messageArr[0] === 'USER') {
        clientArr[id] = {
          user_id: messageArr[1],
          connection: ws,
        };
        id += 1;
      } else if (messageArr[0] === 'CHAT') {
        wss.clients.forEach((client) => {
          if (client !== ws && client.readyState === 1) {
            client.send(message);
          }
        });
      } else if (messageArr[0] === 'GAME') {
        const gamestone = messageArr[1];
        const clientid = messageArr[2].split(':');
        clientArr.forEach((client) => {
          if (client.user_id.localeCompare(clientid[1]) === 0) {
            client.connection.send(`GAME;${gamestone}`);
          }
        });
      }
      console.log('server received: %s', message);
    });
  });
}

function sendtoclient(clientid, message) {
  clientArr.forEach((client) => {
    if (client.user_id.localeCompare(clientid) === 0) {
      client.connection.send(message);
    }
  });
}

module.exports = { launchwsserver, sendtoclient };