const Stock = require('../models/stock');

// GET ALL SHOPS STOCKS
exports.getAllShop = (req, res) => {
  Stock.find()
      .then((stocks) => res.status(200).json(stocks))
      .catch((error) => res.status(404).json({ error }));
};

exports.updateShop = (req, res) => {
  console.log(req.body)
  console.log(req.params)
  Stock.updateOne({ city: req.body.city }, {stock: req.body.stock})
      .then(() => res.status(200).json({ message: 'stock updated' }))
      .catch((error) => res.status(400).json({ error }));
}

//ADD A SHOP
exports.addShop = (req, res) => {
        const newshop = new Stock({
          city: req.body.city,
          stock: req.body.stock,
        });
        newshop.save()
            .then(() => res.status(201).json({ message: 'user created successfully' }))
            .catch((error) => res.status(400).json({ error }));
};