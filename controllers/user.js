const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const User = require('../models/user');

// SIGN UP USER



exports.signup = (req, res) => {
  bcrypt.hash(req.body.password, 10)
      .then((hash) => {
        const newuser = new User({
          username: req.body.username,
          email: req.body.email,
          password: hash,
        });
        newuser.save()
            .then(() => res.status(201).json({ message: 'user created successfully' }))
            .catch((error) => res.status(400).json({ error }));
      })
      .catch((error) => res.status(500).json({ error }));
};

// LOGIN USER
exports.login = (req, res) => {
  User.findOne({ email: req.body.email })
      .then((user) => {
        if (!user) {
          res.status(401).json({ error: 'user not found' });
        }
        bcrypt.compare(req.body.password, user.password)
            .then((valid) => {
              if (!valid) {
                res.status(401).json({ error: 'wrong password' });
              }
              res.status(200).json({
                userId: user.id,
                token: jwt.sign(
                    { userId: user.id },
                    'STEVEBABAISMYSECRETANDIHAVENOSHAMEWHATSOEVER',
                    { expiresIn: '24h' },
                ),
              });
            })
            .catch((error) => res.status(500).json({ error }));
      })
      .catch((error) => res.status(500).json({ error }));
};

// GET ALL USERS
exports.getAllUsers = (req, res) => {
  User.find()
      .then((users) => res.status(200).json(users))
      .catch((error) => res.status(404).json({ error }));
};

// GET USER BY ID
exports.getUserById = (req, res) => {
  User.findOne({ _id: req.params.id })
      .then((oneuser) => res.status(200).json(oneuser))
      .catch((error) => res.status(404).json({ error }));
};

// GET USER BY EMAIL
exports.getUserByEmail = (req, res) => {
  User.findOne({ email: req.params.email })
      .then((oneuser) => res.status(200).json(oneuser))
      .catch((error) => res.status(404).json({ error }));
};

// UPDATE USER BY ID
exports.updateUserById = (req, res) => {
  User.updateOne({ _id: req.params.id }, { ...req.body, _id: req.params.id })
      .then(() => res.status(200).json({ message: 'user updated' }))
      .catch((error) => res.status(400).json({ error }));
};

// DELETE USER BY ID
exports.deleteUserById = (req, res) => {
  User.deleteOne({ _id: req.params.id })
      .then(() => res.status(200).json({ message: 'user deleted' }))
      .catch((error) => res.status(400).json({ error }));
};